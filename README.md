# JMeter Tests for Backoffice

Contains various JMeter test packs and configuration used for Backoffice testing.

- `configuration/` contains property files for various environments.
- `resources/` contains various scripts used by JMeter tests. These are shared for all the various test packs.
- The other directories contains various JMeter test packs, the actual test files (`.jmx`) being in `<test-pack>/src/main/jmeter/`.

## Test Versions
The versions of each test pack should be declared in `jmeter-runner.xml` in [`deploy-back`](https://bitbucket.org/gwynniebee/deploy-back/) repository.


## Running Tests on Bamboo

Selective JMeter test packs are run are part of dev, qa and prod deployment plans. There also exists some Bamboo plans that can be run manually to run JMeter tests:

1. [jmeter test runner DEV](https://gwynniebee.atlassian.net/builds/browse/BACK-JMETERTESTRUNNERBACKDEV) - Runs all test packs in dev environment. **Most useful of all these plans.**
2. [jmeter test runner QA](https://gwynniebee.atlassian.net/builds/browse/BACK-JMETERTESTRUNNERBACKQANEW) - Runs Regression tests (from both master and qa branches) in QA environment.
3. [jmeter test runner PROD](https://gwynniebee.atlassian.net/builds/browse/BACK-BACKOFFICEJMETERTESTRUNNERPRODNEWTHREE) - Runs Production test pack in prod environment.

## Running Tests Locally

### Install JMeter

- Download latest JMeter binaries from [http://jmeter.apache.org/](http://jmeter.apache.org/).
- Extract the package and make sure all files in the `bin/` folder has execute permission. You can do it with `chmod +x *`.
- Add the `bin/` directory to your `$PATH` (preferably add it in your `~/.bash_profile`).

### Interactive Running

You can run a single test interactively using JMeter's GUI.
	
	cd resources/
	# Launch JMeter with dev/qa/prod configuration.
	jmeter -p ../configuration/dev/backoffice.user.properties
	# Now you can open any .jmx file and run individual tests.