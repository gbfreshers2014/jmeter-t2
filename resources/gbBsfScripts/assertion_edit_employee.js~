var myAssertion = {

  assertError:function(failureMessage)
  {
  	OUT.println("[JMETER_ERROR] [" + sampler.getName() + "] [" + sampler.getUrl() + "], AssertionFailureMessage [" + failureMessage + "]");
  	AssertionResult.setFailureMessage(failureMessage) ;
	AssertionResult.setFailure(true);
  },
  
  assertPass:function(logMessage)
  {
	 OUT.println("[JMETER_PASSED] [" + sampler.getName() + "] " + logMessage);
  },
  
  logDebug:function(logMessage)
  {
	OUT.println("[JMETER_DEBUG] [" + sampler.getName() + "] " + logMessage);
  },
  
  isValidISODate:function(date)
  {
  	var isoFormat = "^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[0-1]|0[1-9]|[1-2][0-9])T(2[0-3]|[0-1][0-9]):([0-5][0-9]):([0-5][0-9])(\.[0-9]+)?(Z|[+-](?:2[0-3]|[0-1][0-9]):[0-5][0-9])?$";
  	var dateParts = date.match(isoFormat);
    if (dateParts == null) {
    	return false;
	}
	else {
		return true;
	}
  },

  validateOrder:function(param_Order, response_Order)
  {
		//Verifying Order object now
		for (var key in param_Order) 
		{
  			if (param_Order.hasOwnProperty(key) && key != 'setOrderIdVar' && key != 'setLastUpdateVar' && key != 'createDate' && key != 'lastUpdated' && key !='orderId' && typeof param_Order[key] !== 'object')
  			{
  			    var responseValue = response_Order[key];
				var paramExpectedValue = param_Order[key];
				
				if(typeof responseValue === 'undefined' || responseValue == null || String(responseValue) !== String(paramExpectedValue))
				{
					myAssertion.assertError("response_Order does not have valid " + key + ". Received: " + responseValue + ", Expecting: " + paramExpectedValue);
				}
				else
				{
					myAssertion.assertPass("Verifying Order Key: " + key + ", Expecting: " + paramExpectedValue+", Received: " + responseValue);
				}
  			}
		}
			
		var orderId = response_Order['orderId'];
		var createDate = response_Order['createDate'];
		var updateDate = response_Order['lastUpdated'];
		var setOrderIdVar = param_Order['setOrderIdVar'];
		var setLastUpdateVar = param_Order['setLastUpdateVar'];
    	    	
    	if(typeof setOrderIdVar === 'undefined' || setOrderIdVar == null || setOrderIdVar.length == 0)
		{
			myAssertion.logDebug("No setOrderIdVar is provided. orderId of Order will not be set in any Jmeter variable.");
		}
		else
		{
			vars.put(setOrderIdVar, orderId);
			myAssertion.logDebug("setOrderIdVar is provided. Extracted orderId will set '" + setOrderIdVar + "' as Jmeter variables");
		}
		
		if(typeof orderId === 'undefined' || orderId == null || orderId <= 0)
		{
			myAssertion.assertError("Order.orderId in JSON response is invalid. orderId: " + orderId + ", Expecting > 0");
		}
		else
    	{
    		myAssertion.assertPass("Verifying Order orderId. Valid non-zero orderId received. Received: " + orderId);
    	}
    		
    	if (!myAssertion.isValidISODate(createDate)) {
    		myAssertion.assertError("Response does not have valid createDate date. Received: " + createDate + ", Expecting: Valid date in ISO format");
    	}
    	else
    	{
    		myAssertion.assertPass("Verifying createDate date. Valid ISO format received. Received: " + createDate);
    	}
    		
    	if (!myAssertion.isValidISODate(updateDate)) {
			myAssertion.assertError("Response does not have valid updateDate date. Received: " + updateDate + ", Expecting: Valid date in ISO format");
    	}
    	else
    	{
    		myAssertion.assertPass("Verifying updateDate date. Valid ISO format received. Received: " + updateDate);
    	}
    	
    	if(typeof setLastUpdateVar === 'undefined' || setLastUpdateVar == null || setLastUpdateVar.length == 0)
		{
			myAssertion.logDebug("No setLastUpdateVar is provided. LastUpdate of Order will not be set in any Jmeter variable.");
		}
		else
		{
			vars.put(setLastUpdateVar, updateDate);
			myAssertion.logDebug("setLastUpdateVar is provided. Extracted lastUpdated will set '" + setLastUpdateVar + "' as Jmeter variables");
		}
    		
    	myAssertion.validateOrderAttributes(param_Order, response_Order);
  },
  
  
  validateOrderAttributes:function(param_Order, response_Order)
  {			
		var param_orderAttributes = param_Order['orderAttributes'];
		var response_orderAttributes = response_Order['orderAttributes'];
		
		if(typeof param_orderAttributes !=='undefined' && param_orderAttributes.length > 0)
		{	
			myAssertion.logDebug("Validating OrderAttributes.");
			if(typeof response_orderAttributes ==='undefined' || response_orderAttributes== null || response_orderAttributes.length != param_orderAttributes.length)
			{
				myAssertion.assertError("Response does not have appropriate orderAttributes");
			}
			else
			{
				var alreadyMatched = new Object();
				
				//Check all orderAttributes by using hashing
				for (var index = 0; index < param_orderAttributes.length; index++)
				{
					var param_orderIdAttribute = param_orderAttributes[index];
					var param_attribute = param_orderIdAttribute["attribute"];
					var param_value = param_orderIdAttribute["value"];
					var match = false;
					
					if (typeof param_attribute === 'undefined' || param_attribute ==null || typeof param_value === 'undefined' || param_value ==null)
					{
						continue;
					}
					
					for (var index_response = 0; index_response < response_orderAttributes.length; index_response++)
					{
						var response_orderIdAttribute = response_orderAttributes[index_response];
						var response_attribute = response_orderIdAttribute["attribute"];
						var response_value = response_orderIdAttribute["value"];
						
						if( alreadyMatched[index_response] == 1)
						{
							myAssertion.logDebug("Skipping. Response index already matched: " + index_response);
						}
						else if((String(param_attribute) === String(response_attribute)) && (String(param_value) === String(response_value)))
						{
							match = true;
							alreadyMatched[index_response] = 1;
							break;
						}
					}
					
					if (match)
					{
						myAssertion.assertPass("Verifying orderAttribute. Expecting param_attribute:" + param_attribute + ",  param_value: " + param_value + ", Received response_attribute: " + response_attribute + ", response_value: " + response_value);
					}
					else
					{
						myAssertion.assertError("Response does not have valid orderAttribute. Expecting param_attribute:" + param_attribute + ",  param_value: " + param_value + ", Received response_attribute: " + response_attribute + ", response_value: " + response_value);
					}
				}
			}
		}
  	},
};

var paramsLength = Parameters.length;
var responseCode = prev.getResponseCode();

if (paramsLength == 0)
{
	myAssertion.assertError("Invalid Jmeter test case. Valid JSON as Input param is required for response validation");
}
else if(!responseCode.equals("200"))
{
	myAssertion.assertError("Invalid ResponseCode received. ResponseCode: " + responseCode + ", Expecting: 200");
}
else
{
	myAssertion.logDebug("[" + sampler.getUrl() + "] ====> [" + prev.getResponseDataAsString() + "]");
	
	try
	{
		eval('var parametersJSON = ' + Parameters);
		eval('var apiResponse = ' + prev.getResponseDataAsString());	
	}	 
	catch (e)
	{
		myAssertion.assertError("Response is not a valid JSON Object");
	}
	
	if(typeof apiResponse !== 'undefined' && apiResponse && typeof parametersJSON !== 'undefined' && parametersJSON && !AssertionResult.isFailure())
	{		
		var code = apiResponse['code'];
		var message = apiResponse['message'];
		
		if(typeof code === 'undefined' || code == null || code !== 1)
		{
			myAssertion.assertError("Code in JSON response is invalid. Code: " + code + ", Expecting: 1");
		}
		else
		{
			myAssertion.assertPass("Verifying status Code: " + code + ", Expecting: 1");
		
		if(typeof message === 'undefined' || message == null || String(message) !== 'Success')
		{
			myAssertion.assertError("Error message in JSON response is not valid. Message: " + message + ", Expecting: Success");
		}
		else
		{
			myAssertion.assertPass("Verifying Message: " + message + ", Expecting: Success");
		}
	}
	else
	{
		failureMessage = "apiResponse is not a valid object. apiResponse: " + apiResponse;
		isAssertionFailed = true;
	}
}
